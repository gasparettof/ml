# Import necessary libraries
import matplotlib.pyplot as plt
from sklearn.datasets import load_digits
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
import numpy as np

# Load the dataset
digits = load_digits()
X = digits.data

# Initialize and train the model
model = KMeans(n_clusters=10, random_state=42)
model.fit(X)

# Predict the cluster for each instance
labels = model.predict(X)

# Use PCA to reduce dimensions
pca = PCA(n_components=2)
X_pca = pca.fit_transform(X)

# Plot the clusters
plt.figure(figsize=(10, 8))
for i in range(10):
    cluster = np.where(labels == i)
    plt.scatter(X_pca[cluster, 0], X_pca[cluster, 1], label=f"Cluster {i}", alpha=0.6)
plt.xlabel("PCA Component 1")
plt.ylabel("PCA Component 2")
plt.title("KMeans Clustering of Digits Dataset")
plt.legend()
plt.show()

