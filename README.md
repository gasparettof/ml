# Machine Learning Examples with Scikit-Learn

This repository contains examples of machine learning models implemented using the scikit-learn library in Python. The examples cover the following models:
1. Linear Regression
2. Classification with Logistic Regression
3. Clustering with KMeans

## Linear Regression

The `linear_regression.py` script demonstrates how to perform linear regression on the California Housing dataset. The mean squared error of the model is calculated to evaluate its performance. 

A scatter plot comparing the actual and predicted values is generated to visualize the accuracy of the model. The red line represents a perfect prediction, helping to see how close the predictions are to the actual values.

## Classification with Logistic Regression

The `classification.py` script shows how to perform classification using logistic regression on the Iris dataset. The accuracy of the model is calculated to evaluate its performance. 

A confusion matrix is generated to show how well the model performs in classifying the different categories. The matrix displays the counts of true positives, true negatives, false positives, and false negatives for each class, providing insight into the classification performance.

## Clustering with KMeans

The `clustering.py` script demonstrates the use of the KMeans algorithm for clustering the digits dataset. The silhouette score is used to evaluate the quality of the clusters.

A PCA is used to reduce dimensions and plot the clusters to visualize the grouping of data points. Each color in the scatter plot represents a different cluster, helping to understand the separation and cohesion of the data points within clusters.

## Requirements

- Python 3.x
- scikit-learn
- matplotlib
- seaborn

## Installation

You can install the necessary libraries using:

```bash
pip install -r requirements.txt

